// Object KeywordClass
// class เป็นการกำหนดคุณสมบัติของวัตถุสิ่งใดสิ่งหนึ่ง
// Object ก็เป็นการเรียกใช้ Class นั้นๆโดยที่ object นั้นๆจะมีคุณสมบัติเหมือนกับ class ที่เรียกใช้ทุกประการ
// จาก google

import java.util.Random

class LuckDispatcher {                    
    fun getNumber() {                      
        var objRandom = Random()
        println(objRandom.nextInt(90))
    }
}

//แต่ใน Kotlin ไม่จำเป็นต้องประกาศ class ก็ได้ ประกาศ Object ตรงๆได้เลย คล้ายๆใน js ที่ใส่ {} ก็กลายเป็น object และ {cat:4,dog:1} <- object ใน js นะ

fun rentPrice(standardDays: Int, festivityDays: Int, specialDays: Int): Unit {  
    val dayRates = object {                                                     
        var standard: Int = 30 * standardDays
        var festivity: Int = 50 * festivityDays
        var special: Int = 100 * specialDays
    }

    val total = dayRates.standard + dayRates.festivity + dayRates.special       

    print("Total price: $$total")                                               

}

object DoAuth {                                                 
    fun takeParams(username: String, password: String){         
        println("input Auth parameters = $username:$password")
    }
}

class BigBen {                                  
    companion object Bonger {                   //เหมือนๆกับการประกาศ static ของ java
        fun getBongs(nTimes: Int) {            
            for (i in 1 .. nTimes) {
                print("BONG ")
            }
        }
    }
}

fun main() {
    val d1 = LuckDispatcher()             
    val d2 = LuckDispatcher()
    
    d1.getNumber()                        
    d2.getNumber()
	
    rentPrice(10, 2, 1)
	
    DoAuth.takeParams("foo", "qwerty")                          
    BigBen.getBongs(12)                         
}