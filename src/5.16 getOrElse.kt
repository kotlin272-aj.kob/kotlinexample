//getOrElse เป็นการเข้าถึง list โดยสามารถกำหนดค่าพื้นฐานตอนที่ไม่สามารถเข้าถึงได้ เช่น [0,1,2] เข้าถึง index ที่ 10 เมื่อไม่เจอเราสามารถตั้งได้ว่าจะให้มัน return ไรกลับไปแทน 

fun main() {
	val list = listOf(0, 10, 20)
	println(list.getOrElse(1) { 42 })    // เข้าถึง index 1 เจอ จะ return ค่ามันออกมา
	//return 10
	
	println(list.getOrElse(3) { "NotFound" })   // เข้าถึง index ที่ 3 ไม่เจอ return ตัวที่เรากำหนดไว้
	//return NotFound

	val map = mutableMapOf<String, Int?>()	// สร้าง map เปล่าขึ้นมา
	println(map.getOrElse("x") { 1 })       // เข้าถึง key "x"
	// return 1

	map["x"] = 3							// กำหนด key x มีค่าคือ 3
	println(map.getOrElse("x") { 1 })       // เข้าถึง key "x" เจอ return ค่าออกมา
	// return 3

	map["x"] = null
	println(map.getOrElse("x") { 1 })       // เข้าถึง key "x" ไม่เจอ เพราะเป็น null(ค่าว่าง)
	// return 1
}