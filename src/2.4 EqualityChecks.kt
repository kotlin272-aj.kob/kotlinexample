fun main(args: Array<String>) {

	val authors = setOf("Shakespeare", "Hemingway", "Twain")
	val writers = setOf("Twain", "Shakespeare", "Hemingway")

	println(authors == writers)   // return true ตรวจสอบแค่ว่ามีแค่เดียวกันไหม
	println(authors === writers)  // return false ตรวจสอบว่าค่ามีตรงกันและมีตำแหน่งเดียวกันไหม

}