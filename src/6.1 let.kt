//let
//ใช้ let ในการประกาศให้ทำใน scope {} นั้นๆ และสามารถเช็คค่า null ได้
//เมื่อใช้ let มันจะทำการ return เป็นค่าสุดท้ายของ scope นั้น
//object ที่ใช้ let ใน scope จะใช้ it เป็นตัวแทน


val empty = "test".let {               // string "test" เรียกใช้ let
    customPrint(it)                    // print คำว่า test ออกมา ที่ตอนนี้ใช้ตัวแปร it แทน
    it.isEmpty()                       // เช็คว่าวว่างไหม return true false
}
println(" is empty: $empty")
//return false

fun printNonNull(str: String?) {
    println("Printing \"$str\":")

    str?.let {                         // ประกาศ let เพื่อหลีกเลี่ยง null 
        print("\t")
        customPrint(it)
        println()
    }
}
fun main(){
	printNonNull(null)					// ส่งค่า null ทำให้ไม่ทำงานในตอนนี้ใช้ let
	//output
	//Printing "null":
	
	printNonNull("my string")			// ส่งค่า string ทำงานปกติ
	//output
	//Printing "my string":
	//	MY STRING
}