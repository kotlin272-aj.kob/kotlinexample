//filter จะคัดกรองค่าของใน list คัดจากการเช็คว่าเป็น true หรือ false

val numbers = listOf(1, -2, 3, -4, 5, -6)      // สร้าง list

val positives = numbers.filter { x -> x > 0 }  // คัดกรองเอาแต่ที่มากกว่า 0 positive จะเหลือแค่ 1

val negatives = numbers.filter { it < 0 }      // คัดกรองเอาแต่ที่น้อยกว่า 0 negatives ถ้าไม่ใช้ lambda ตัวแปรจะแทนด้วย it 