//map จะแปลงค่าทุกตัวของข้างใน list ได้ 

val numbers = listOf(1, -2, 3, -4, 5, -6)     // 1

val doubled = numbers.map { x -> x * 2 }      // // แปลงให้ตัวแปรทุกตัว * ด้วย 2

val tripled = numbers.map { it * 3 }          // แปลงให้ตัวแปรทุกตัว * ด้วย 3 ถ้าไม่ใช้ lambda ตัวแปรจะแทนด้วย it 

	fun main() {
	
}