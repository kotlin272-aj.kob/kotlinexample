// Partition
// คล้ายๆ map แต่จะสามารถแยก list ออกมาเป็น 2 อันได้ ถ้าตรงเงื่อนไขหรือ return true จะอยู่ใน list อันแรก ถ้าไม่ตรงจะอยู่ใน list อัน2 

val numbers = listOf(1, -2, 3, -4, 5, -6)                // สร้าง list

val evenOdd = numbers.partition { it % 2 == 0 }           // สร้าง list ใหม่ที่มีแต่เลขคู่ (เหมือน map)
val (positives, negatives) = numbers.partition { it > 0 } // แยก list ออกเป็น 2 อัน อันแรกเลขคืออันที่มมากกว่า 0 (บวก) อัน 2 คือ <= 0