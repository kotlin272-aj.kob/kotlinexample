// Default Parameter Values and Named Arguments
//-----------------------------
fun printMessage(message: String): Unit {                               // ตรง Unit เป็นการบอกว่าไม่ return ค่า 
    println(message)
}

fun printMessageWithPrefix(message: String, prefix: String = "Info") {  // ตรงตัวแปร Prefix เราสามารถกำหนดค่าให้ได้
    println("[$prefix] $message")
}

fun sum(x: Int, y: Int): Int {                                          // รับ parameter 2 ตัว และ return เป็น Int
    return x + y
}

fun multiply(x: Int, y: Int) = x * y                                    // จะไม่ใส่ก็ได้ว่าจะ return อะไร โปรแกรมจะเดาให้เอง

fun main2() {
    printMessage("Hello")                                               // 5                    
    printMessageWithPrefix("Hello", "Log")                              // 6
    printMessageWithPrefix("Hello")                                     // เรียกใช้ฟังก์ชั่นที่มีการกำหนดค่าไว้ตั้งแต่เริ่ม
    printMessageWithPrefix(prefix = "Log", message = "Hello")           // เรียกใช้ฟังก์ชั่นแบบกำหนดค่าให้ (ไม่เรียง parameter)
    println(sum(1, 2))                                                  // 9
    println(multiply(1, 2))                                              
}

//-----------------------------
//Infix Functions
//-----------------------------

fun main() {

  infix fun Int.times(str: String) = str.repeat(this)        // ประกาศฟังก์ชั่น ที่ค่าแรกจะรับ Int ตามด้วยชื่อฟังก์ชั่น times และรับ Stringมา
  println(2 times "Bye ")                                    // เรียกใช้ฟังก์ชั่นแบบ infix

  val pair = "Ferrari" to "Katrina"                          // เรียกใช้ฟังก์ชั่นแบบ infix ที่โปรแกรมมีไว้ให้
  println(pair)

  infix fun String.onto(other: String) = Pair(this, other)   // 4
  val myPair = "McLaren" onto "Lucas"
  println(myPair)

  val sophia = Person("Sophia")
  val claudia = Person("Claudia")
  sophia likes claudia                                       // เรียกใช้ฟังก์ชั่นที่อยู่ใน class
  print(sophia.likedPeople[0].name) 						 // ลองแสดงผลค่าดูว่าเพิ่มจริงหรือเปล่า
}

class Person(val name: String) {
  val likedPeople = mutableListOf<Person>()
  infix fun likes(other: Person) { likedPeople.add(other) }  // 
}


//-----------------------------
//Operator Functions
//-----------------------------
operator fun Int.times(str: String) = str.repeat(this)       // ประกาศเหมือน Infix function แต่อันนี้จะใช้ operator
//println(2 * "Bye ")                                        // การเรียกใช้ตัวแปรจะแทนด้วย operator(เครื่องหมาย)แทน
operator fun Int.plus(str: String) = str.repeat(this)		 //เช่น times คือ * plus คือ +


//-----------------------------
//Functions with vararg Parameters
//-----------------------------
fun printAll(vararg messages: String) {                            // ประกาศฟังก์ชั่นที่รับ parameterประเภทvararg
    for (m in messages) println(m)
}
//printAll("Hello", "Hallo", "Salut", "Hola", "ง่วงนอน")              // ตอนเรียกใช้สามารถใส่ parameter กี่ตัวก็ได้

fun printAllWithPrefix(vararg messages: String, prefix: String) {  // ประกาศฟังก์ชั่นที่รับ parameterประเภทvararg และประเภทอื่นอีก1ตัว
    for (m in messages) println(prefix + m)
}
/*printAllWithPrefix(
    "Hello", "Hallo", "Salut", "Hola", "ง่วงนอน",
    prefix = "Greeting: "                                          // ตอนเรียกใช้สามารถใส่ parameter กี่ตัวก็ได้
) */															   // แต่ตัวแปรอื่นที่เพิ่มเข้ามาต้องกำหนดชื่อตัวแปรให้ด้วย

fun log(vararg entries: String) {
    printAll(*entries)                                             // การใช้ตัวแปร vararg ใช้ *ชื่อตัวแปร
}