//Higher-Order Functions ฟังก์ชันที่ใช้ฟังก์ชันอื่นเป็น parameter / หรือ return ค่าจากฟังก์ชัน

fun calculate(x: Int, y: Int, operation: (Int, Int) -> Int): Int {  // ประกาศฟังก์ชั่นรับ x y และ รับฟังก์ชั่น operation ที่รับ parameter int 2 ตัว และ return ค่ากลับเป็น Int
    return operation(x, y)                                          // เรียกใช้ฟังก์ชั่นที่รับมา
}

fun sum(x: Int, y: Int) = x + y                                     // ฟังก์ชั่นรับ x y และ return ค่า x+y

fun main() {
    val sumResult = calculate(4, 5, ::sum)                          // ใช้ :: บอกถึงการเรียกชื่อของฟังก์ชั่น
    val mulResult = calculate(4, 5) { a, b -> a * b }               // สร้างฟังก์ชั่นแล้วส่งไปให้ฟังก์ชั่น calculate
    println("sumResult $sumResult, mulResult $mulResult")
}

fun operation(): (Int) -> Int {                                     // ฟังก์ชั่นรับค่าเป็น int และ return กลับเป็น Int
    return ::square
}

fun square(x: Int) = x * x                                          // ฟังก์ชั่นรับ x เป็น Int และ return ค่า x*x กลับโดยไม่ต้องใส่ return x*x

fun main() {
    val func = operation()                                          // ให้ฟังก์ชั่น square ที่อยูใน operation แก่ func
    println(func(2))                                                // ใช้ฟังก์ชั่น func (ก็คือไปใช้ฟังก์ชั่น square)
}