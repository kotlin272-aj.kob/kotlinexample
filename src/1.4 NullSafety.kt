fun main() {
	var neverNull: String = "This can't be null"            // ถ้าประกาศแบบปกติไม่สามารถเป็นค่า null ได้

	neverNull = null                                        // มันจะแดงในทันที

	var nullable: String? = "You can keep a null here"      // แต่ถ้าประกาศประเภทตัวแปรตามด้วย? จะสามารถเป็น null ได้

	nullable = null                                         // 4

	var inferredNonNull = "The compiler assumes non-null"   // ไม่ประกาศเลย ปล่อยให้โปรแกรมเดาประเภทเองก็เป็น null ไม่ได้

	inferredNonNull = null                                  // 6

	fun strLength(notNull: String): Int {                   // 7 รับค่าเป็นประเภท string ปกติ
		return notNull.length
	}

	strLength(neverNull)                                    // ส่งค่าที่เป็น string ปกติส่งได้
	strLength(nullable)										// ส่งค่าที่เป็น string? ส่งไม่ได้

}