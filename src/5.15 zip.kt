//zip รวม list เข้าสู่ list ใหม่ โดยพื้นฐาน zip จะรวมกันด้วยการ pair

val A = listOf("a", "b", "c")                  // สร้าง list ตัวที่1
val B = listOf(1, 2, 3, 4)                     // สร้าง list ตัวที่2

val resultPairs = A zip B                      // list ตัวที่1เชื่อมตัวที่2
// return [(a, 1), (b, 2), (c, 3)]

val resultReduce = A.zip(B) { a, b -> "$a$b" } // list ตัวที่1เชื่อมตัวที่2 แบบใช้ lambda ในการกำหนดการเชื่อม
//เชื่อมแบบ aติดb
//[a1, b2, c3]