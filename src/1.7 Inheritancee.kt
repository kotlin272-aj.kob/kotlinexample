// การสืบทอดกรอบ
open class Dog {                // ประกาศคลาส่ที่มี open ตามหน้า หมายถึงจะอนุญาติให้สามารถ inheritance
    open fun sayHello() {       // 2
        println("wow wow!")
    }
}

class Yorkshire : Dog() {       // หลังจาก class ชื่อ class จะตามด้วย : ชื่อคลาสที่จะ inherit
    override fun sayHello() {   // override จะทับฟังก์ชั่นเก่าที่คลาสแม่(dog)มีอยู่
        println("wif wif!")
    }
}

open class Tiger(val origin: String) {
    fun sayHello() {
        println("A tiger from $origin says: grrhhh!")
    }
}

class SiberianTiger : Tiger("Siberia") 

open class Lion(val name: String, val origin: String) {
    fun sayHello() {
        println("$name, the lion from $origin says: graoh!")
    }
}

class Asiatic(name: String) : Lion(name = name, origin = "India") // สามารถกดหนดค่าเริ่มต้นให้เลยก็ได้	

fun main() {
    val dog: Dog = Yorkshire()
    dog.sayHello()
}