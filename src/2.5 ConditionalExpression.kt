// if else ปกติเหมือนภาษาทั่วไป

fun max(a: Int, b: Int) = if (a > b) a else b         // 1


fun main(args: Array<String>) {
	println(max(99, -42))
	
	val a :Int = 120
	val b :Int = 140
	if(a>b){
		println("a > b")
	}else if (b>a){
		println("a > b")
	}else{
		println("a = b")
	}
}