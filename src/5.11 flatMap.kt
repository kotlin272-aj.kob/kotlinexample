// flatMap เป็นการทำให้ list สามารถทำซ้ำตัวค่าดิม เช่น list มี [1,2] ทำให้เป็น [1,1,2,2] ตัวแปรเดิมคือ 1 กับ 2
val numbers = listOf(1, 2, 3)                        // 1

val tripled = numbers.flatMap { listOf(it, it, it) } // นำเอาค่าเดิมเพิ่มขึ้น3ค่า
//return  [1, 1, 1, 2, 2, 2, 3, 3, 3]


val tripled = numbers.flatMap { listOf(it+1, it*2) } // สร้างค่าเดิมโดยเอาค่าเก่า  + 1 และ *2
// return [1, 2, 2, 2, 3, 4, 3, 4, 6]