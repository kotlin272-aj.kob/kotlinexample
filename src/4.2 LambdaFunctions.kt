// จะสร้างฟังก์ชั่นที่ทำให้ตัวหนังสือเป็นตัวพิมพ์ใหญ่เป็นตัวอย่างทั้งหมด
// ดังนั้นแล้วฟังก์ชั่นนี้จะรับ String และ return ค่ากลับเป็น string หรือคือ  String -> String ใน Lambda
// โดยตัวแปรที่รับมาจะแทนด้วย it (ถ้าไม่กำหนดค่า)

val upperCase1: (String) -> String = { str: String -> str.toUpperCase() } // รับค่ามาแล้วกำหนดค่าเป็น str

val upperCase2: (String) -> String = { str -> str.toUpperCase() }         // รับค่ามาแล้วกำหนด str โดยไม่กำหนดประเภท (ให้โปรแกรมกำหนดหรือเดาประเภทเอง)

val upperCase3 = { str: String -> str.toUpperCase() }                     // เหมือนๆกัน

// val upperCase4 = { str -> str.toUpperCase() }                          // 4

val upperCase5: (String) -> String = { it.toUpperCase() }                 // ถ้าไม่กำหนดว่าค่าที่รับมา(String)เป็นค่าอะไร จะใช้ it ในการแทนค่านั้นแทน

val upperCase6: (String) -> String = String::toUpperCase                  // ถ้าเป็นฟังก์ชั่นเดี่ยว(ฟังก์ชั่นที่รับ parameter ตัวเดียว)จะสามารถใช้ ::เพื่อเรียกฟังก์ชั่นนั้นได้เลย

fun main(){
	println(upperCase1("hello"))
	println(upperCase2("hello"))
	println(upperCase3("hello"))
	println(upperCase5("hello"))
	println(upperCase6("hello"))
}