package org.kotlinlang.play         // มีหรือไม่มีก็ได้ ถ้าไม่มีโปรแกรมจะใส่ให้เอง

fun main() {                        // ต้องกำหนดเพื่อเป็นตัวทำงานหลัก
    println("Hello, World!")        // แสดงผล Hello World
	println("newline")
	print("inline")
	print(" inline")
}

//fun main(args: Array<String>) {	// ถ้า Kotlin version 1.3 ขึ้นไป ต้องใส่ parameter args ด้วย
//    println("Hello, World!")
//}