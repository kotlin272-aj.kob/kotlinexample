class Customer                                  // ประกาศ class แบบไม่มีไรเลย

class Contact(val id: Int, var email: String)   // ประกาศ class แบบรับตัวแปร

fun main() {

    val customer = Customer()                   // เรียกใช้ class แบบไม่มีไรเลย
    
    val contact = Contact(1, "mary@gmail.com")  // เรียกใช้ class แบบใส่ parameter ไปด้วย

    println(contact.id)                         // เรียก id ออกมาก้ (ปริ้น 1 )
    contact.email = "jane@gmail.com"            // บันทึกค่าลงตัวแปรได้ตามตกปิ
}