// ใน Kotlin สามารถประกาศ List แบบ mutable ได้
// โดย List ปกติจะเป็น readonly อ่านได้อย่างเดียว
// mutable คือ อ่านและแก้ไขได้

val systemUsers: MutableList<Int> = mutableListOf(1, 2, 3)        // ประกาศ list แบบ mutable
val sudoers: List<Int> = systemUsers                              // ประกาศ list อ่านได้อย่างเดียว

fun addSudoer(newUser: Int) {                                     // ฟังก์ชั่นไว้เพิ่มเข้า list mutable
    systemUsers.add(newUser)                      
}

fun getSysSudoers(): List<Int> {                                  // 4
    return sudoers
}

fun main() {
    addSudoer(4)                                                  // เพิ่มเข้า list mutable ไม่ error 
    println("Tot sudoers: ${getSysSudoers().size}")               // 6
    getSysSudoers().forEach {                                     // 7
        i -> println("Some useful info on user $i")
    }
    // getSysSudoers().add(5) <- Error!                           // sudoers เป็น list แบบ readonly ไม่สามารถเพิ่มค่าได้ทำให้ error
}