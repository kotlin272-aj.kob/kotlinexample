// Count ไว้นับจำนวน

val numbers = listOf(1, -2, 3, -4, 5, -6)            // สร้าง list

val totalCount = numbers.count()                     // นับจำนวน
val evenCount = numbers.count { it % 2 == 0 }        // นำจนวนที่หาร 2 ลงตัว