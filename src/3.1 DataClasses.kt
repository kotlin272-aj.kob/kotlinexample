// data class จะทำให้เราใช้งานง่ายขึ้นมันจะสร้างฟังก์ชั่นที่ใช้บ่อยๆอัตโนมัติเช่น getter setter tostring coppy ไวให้เลย

data class User(val name: String, val id: Int)             // กำหนด class เป็น data class

fun main() {
    val user = User("Alex", 1)
    println(user)                                          // ใช้ Method toString ที่สร้างอัตโนมัติ

    val secondUser = User("Alex", 1)
    val thirdUser = User("Max", 2)

    println("user == secondUser: ${user == secondUser}")   // ใช้ Method equals ที่สร้างอัตโนมัติ
    println("user == thirdUser: ${user == thirdUser}")

    println(user.hashCode())                               // 4
    println(thirdUser.hashCode())

    // copy() function
    println(user.copy())                                   // 5
    println(user.copy("Max"))                              // 6
    println(user.copy(id = 2))                             // 7
    
    println("name = ${user.component1()}")                 // เรียกตัวแปรตามลำดับก็ได้
    println("id = ${user.component2()}")
}