//when เหมือนๆ switch case เลย
//แต่ kotlin จะใช้ when ซึ่งรับแค่คัวแปรมาแล้ว ใส่ค่าได้เลยว่า ถ้าเป็นค่านี้จะออกเป็นอะไร แบบในตัวอย่าง
 
fun main() {
    cases("Hello")
    cases(1)
    cases(0L)
    cases("hello")
}

fun cases(obj: Any) {                                
    when (obj) {                                     // รับค่า obj
        1 -> println("One")                          // ถ้า obj เป็น 1 
        "Hello" -> println("Greeting")               // ถ้า obj เป็น hello 
        is Long -> println("Long")                   // ถ้า obj เป็นประเภท Long
        !is String -> println("Not a string")        // ถ้าไม่เป็น String
        else -> println("Unknown")                   // อย่างอื่น
    }   
}


fun whenAssign(obj: Any): Any {
    val result = when (obj) {                   // 1
        1 -> "one"                              // 2
        "Hello" -> 1                            // 3
        is Long -> false                        // 4
        else -> 42                              // 5
    }
    return result
}
