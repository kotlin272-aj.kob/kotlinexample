// เหมือนกับ let เลย  แค่ของ let ใช้ it ในการเข้าถึง object ที่ใช้ แต่ run จะใช้ this (มันนาจาออโต้เองมั้ง)

fun getNullableLength(ns: String?) {
    println("for \"$ns\":")
    ns?.run {                                                  // ใช้ run
        println("\tis empty? " + isEmpty())                    // เรียกฟังก์ชั่น isEmpty() มาจาก ns 
        println("\tlength = $length")                           
        length                                                 // เรียกฟังก์ชั่น length มาจาก ns 
    }
}
getNullableLength(null)
getNullableLength("")
getNullableLength("some string with Kotlin")