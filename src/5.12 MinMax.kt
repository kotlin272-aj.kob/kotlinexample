val numbers = listOf(1, 2, 3) //ประกาศ list
val empty = emptyList<Int>() //ประกาศ list แบบว่างๆ

fun main(){
	println("Numbers: $numbers, min = ${numbers.min()} max = ${numbers.max()}") // แสดงค่า min - max ออกมา
	println("Empty: $empty, min = ${empty.min()}, max = ${empty.max()}")        // ถ้าใน list เป็นว่าง จะ return เป็น null
}