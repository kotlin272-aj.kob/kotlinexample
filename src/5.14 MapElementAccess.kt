//Map Element Access
//การเข้าถึง element(ค่า) ของ map โดยใช้ [] ถ้าหาเจอจะ  return value กลับมา ถ้าไม่เจอจะ return null

val map = mapOf("key" to 42)

val value1 = map["key"]                                     // เรียก key ได้ค่า 42
val value2 = map["key2"]                                    // เรียก key 2 ไม่เจอค่า return null

val value3: Int = map.getValue("key")                       // เรียก key ได้ค่า 42


//withDefault ไว้กำหนดค่าพื้นฐานที่จะ return กลับมาเมื่อไม่เจอ
val mapWithDefault = map.withDefault { k -> k.length }		// เมื่อไม่เจอตั้งให้  return เป็นขนาดของ key

val value4 = mapWithDefault.getValue("key2")                // ไม่เจอ key2 return ขนาดของkey (4)

fun main(){	
	try {
		//getValue จะ return ค่าทางเจอตาม key ที่หา ถ้าไม่เจอจะ throw error 
	    map.getValue("anotherKey")                              // หา key ไม่เจอ โยน  error NoSuchElementException
	} catch (e: NoSuchElementException) {
	    println("Message: $e")
	}
}