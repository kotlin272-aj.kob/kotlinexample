//Enum ใช้ในการกำหนด คำชุดนึง ที่ข้างในมีค่าแตกต่างกันเช่น state(สถานะ)  directions(ทิศทาง ซ้ายขวา) 
//Example1
enum class State {
    IDLE, RUNNING, FINISHED                           // กำหนดสถานะ ใช้ในการบอกด้ว่าตอนนี้อยู่สถานะไหน
}

fun main() {
    val state = State.RUNNING                         // กำหนดให้ state อยู่ในสถานะ RUNNING
    val message = when (state) {                      // ตรวจสอบว่าอยู่ใน state ไหน
        State.IDLE -> "It's idle"
        State.RUNNING -> "It's running"
        State.FINISHED -> "It's finished"
    }
    println(message)
}

//Example2
enum class Color(val rgb: Int) {                      // 1
    RED(0xFF0000),                                    // 2
    GREEN(0x00FF00),
    BLUE(0x0000FF),
    YELLOW(0xFFFF00);

    fun containsRed() = (this.rgb and 0xFF0000 != 0)  // 3
}

fun main2() {
    val red = Color.RED
    println(red)                                      // 4
    println(red.containsRed())                        // 5
    println(Color.BLUE.containsRed())                 // 6
}