// Find Last จะ return ค่าของตัวแรกที่เจอกลับมา ถ้าไม่เจอจะ โยน NoSuchElementException ออกมา
	
fun main(){
	val numbers = listOf(1, -2, 3, -4, 5, -6)            // สร้าง list

	val first = numbers.first()                          // หาตัวแรก
	val last = numbers.last()                            // หาตัวสุดท้าย
	
	val firstEven = numbers.first { it % 2 == 0 }        // หาตัวแรกที่เป็นเลขคู่
	val lastOdd = numbers.last { it % 2 != 0 }           // หาตัวสุดท้ายที่เป็นเลขคี่
	
	//firstOrNull, lastOrNull ต่างกับ First Last ที่ถ้าไม่เจอจะ return null
	val words = listOf("foo", "bar", "baz", "faz")         // สร้าง List
	val empty = emptyList<String>()                        // สร้างList เปล่าๆ
	
	val first = empty.firstOrNull()                        // หาตัวแรก
	val last = empty.lastOrNull()                          // หาตัวสุดท้าย
	
	val firstF = words.firstOrNull { it.startsWith('f') }  // หาตัวแรกที่ขึ้นต้นด้วย f
	val firstZ = words.firstOrNull { it.startsWith('z') }  // หาตัวแรกที่ขึ้นต้นด้วย z ไม่เจอจะ return null ออกมา
	val lastF = words.lastOrNull { it.endsWith('f') }      // หาตัวสุดท้ายที่ตัวท้ายเป็น f ไม่เจอจะ return null ออกมา
	val lastZ = words.lastOrNull { it.endsWith('z') }      // หาตัวสุดท้ายที่ตัวท้ายเป็น z
}