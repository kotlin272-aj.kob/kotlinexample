//Kotlin จะยอมให้สามารถขยาย(extend) function และ properties ได้้
//มันจะเหมือนการประกาศฟังก์ชั่นทั่วไป แต่เพิ่มคือต้องบอกด้วยว่า extend มาจาก class ไหน
//ปล มีความมั่วมหาแห่งความสูง

data class Item(val name: String, val price: Float)                                   // 1  

data class Order(val items: Collection<Item>)  

fun Order.maxPricedItemValue(): Float = this.items.maxBy { it.price }?.price ?: 0F    // เป็นการบอกว่า extend จาก class Order
fun Order.maxPricedItemName() = this.items.maxBy { it.price }?.name ?: "NO_PRODUCTS"

val Order.commaDelimitedItemNames: String                                             // 3
    get() = items.map { it.name }.joinToString()

fun main() {

    val order = Order(listOf(Item("Bread", 25.0F), Item("Wine", 29.0F), Item("Water", 12.0F)))
    
    println("Max priced item name: ${order.maxPricedItemName()}")                     // 4
    println("Max priced item value: ${order.maxPricedItemValue()}")
    println("Items: ${order.commaDelimitedItemNames}")                                // 5

}

fun <T> T?.nullSafeToString() = this?.toString() ?: "NULL"  // 1