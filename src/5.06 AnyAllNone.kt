// Any ถ้าตรงเงื่อนไขตัวดีจะ return กลับเป็น true
val numbers = listOf(1, -2, 3, -4, 5, -6)            // 1

val anyNegative = numbers.any { it < 0 }             // 2

val anyGT6 = numbers.any { it > 6 }                  // 3

// All ถ้าตรงเงื่อนไขทั้งหมดจะ return ค่าเป็น true
val numbers = listOf(1, -2, 3, -4, 5, -6)            // 1

val allEven = numbers.all { it % 2 == 0 }            // 2

val allLess6 = numbers.all { it < 6 }                // 3

// none ถ้าไม่ตรงเงื่อนไขทั้งหมดจะ return ค่าเป็น true
val numbers = listOf(1, -2, 3, -4, 5, -6)            // 1

val allEven = numbers.all { it % 2 == 0 }            // 2

val allLess6 = numbers.all { it < 6 }                // 3

fun main(args: Array<String>) {
	
}