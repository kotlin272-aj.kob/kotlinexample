//with เป็น non-extension function
//สามารถเข้าถึงตัวแปรได้แบบย่อๆ โดยการเอาตัวแปรนั้นมาเป็น  parameter ข้างในของ scope นั้น จะสามารถละเว้นการเรียกชื่อตัวแปรได้
// งงเด้ ดูตัวอย่างเอารึกัน

with(configuration) {				//ประกาศ with มี parameter configuration
    println("$host:$port")			//ใน scope นี้เราสามารถเรียกตัวแปร host , port ที่อยู่ใน configuration ได้เลย
}

// instead of:
println("${configuration.host}:${configuration.port}")   //ตามปกติต้องเรียกแบบนี้