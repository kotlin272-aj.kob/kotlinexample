// Sealed class จะทำให้ class มีคุณสมบัตรต้องเรียกจาก subclass(คลาสลูก) และจะสามารถ Inherit(สืบทอด)ได้เพียงในไฟล์นี้เท่านั้น
sealed class Mammal(val name: String)                                                   // 1

class Cat(val catName: String) : Mammal(catName)                                        // สืบทอดจาก Mammal
class Human(val humanName: String, val job: String) : Mammal(humanName)					// สืบทอดจาก Mammal

fun greetMammal(mammal: Mammal): String {
    when (mammal) {                                                                     // ตรวจสอบว่า class ใด
        is Human -> return "Hello ${mammal.name}; You're working as a ${mammal.job}"    // ถ้าเป็น Human
        is Cat -> return "Hello ${mammal.name}"                                         // ถ้าเป็น Cat     
    }                                                                // ไม่จำเป็นต้องมี else เพราะว่า ไม่มี subclass อื่นอีกแล้ว
}

fun main() {
    println(greetMammal(Cat("Snowy")))
    println(greetMammal(Human("NaewKung","Stalker" )))
    //Mammal("namew")   // error เพราะว่า sealed class ต้องเรียกจาก subclass เท่านั้น
}