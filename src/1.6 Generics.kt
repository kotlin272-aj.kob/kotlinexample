// Generics มีไว้เพราะไม่่รู้ว่าประเภทที่รับมาจะเป็นประเภทอะไร string int double float ...
// ตัวอย่างเช่นใน class list ไม่แน่ว่า ผู้ใช้จะใส่ประเภทอะไรเข้ามาใน list บางทีจะเป็น [2,3] หรืิอ ["k","k"]

class MutableStack<E>(vararg items: E) {              // 1

  private val elements = items.toMutableList()

  fun push(element: E) = elements.add(element)        // 2

  fun peek(): E = elements.last()                     // 3

  fun pop(): E = elements.removeAt(elements.size - 1)

  fun isEmpty() = elements.isEmpty()

  fun size() = elements.size

  override fun toString() = "MutableStack(${elements.joinToString()})"
}
fun <E> mutableStackOf(vararg elements: E) = MutableStack(*elements)

fun main() {
  val stack = mutableStackOf(0.62, 3.14, 2.7)
  println(stack)
}