val shuffled = listOf(5, 4, 2, 1, 3)     // สร้าง list

val natural = shuffled.sorted()          // เรียงค่าใน list (จากน้อยไปมาก)
//[1, 2, 3, 4, 5]

val inverted = shuffled.sortedBy { -it } // เรียงค่าแบบตรงข้าม (จากมากไปน้อย)
//[5, 4, 3, 2, 1]