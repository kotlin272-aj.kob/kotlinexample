//Find จะ return ค่าของตัวแรกที่เจอกลับมา ถ้าไม่เจอจะ return null
val words = listOf("Lets", "find", "something", "in", "collection", "somehow")  // สร้าง List

val first = words.find { it.startsWith("some") }                                // ค้นหาคำที่เริ่มต้นด้วย some

//-------------------------------
//FindLast จะ return ค่าของสุดท้ายที่เจอกลับมา
val last = words.findLast { it.startsWith("some") }                             // ค้นหาคำที่เริ่มต้นด้วย some โดยเริ่มหาจากตัวท้ายสุดถึงตัวแรก

val nothing = words.find { it.contains("nothing") }                             // ถ้าหาไม่เจอเลยจะ return null